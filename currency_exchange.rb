# ruby 2.3.0
# evaluation task

# this method accepts a non-negative amount of INR money as an input, return the exchange value for different currency.
def change_for(inr_amt)
  	begin 
		return "pass non-negative amount" if inr_amt < 0
		currency_value = {quarter: 25, dime: 10, nickel: 5, penny: 1}
		currency_value.each do |k, v|
			currency_value[k] = v*inr_amt
		end
	rescue StandardError => e 
		e.message
	end
end

puts change_for(2)

# this method accept the currency and it's amount to be exchanged, returns the exchange value of different currency.
def change_from(currency, amount)
	currency_rate = {"AED": 3.672817, "AFN": 69.5, "ALL": 109.38, "AMD": 482.415, "ANG": 1.784605, "AOA": 182.011, "ARS": 18.8426, "AUD": 1.252233, "AWG": 1.791246, "AZN": 1.69, "BAM": 1.59555, "BBD": 2, "BDT": 83.125255, "BGN": 1.598044, "BHD": 0.376982, "BIF": 1770, "BMD": 1, "BND": 1.320252, "BOB": 6.90865, "BRL": 3.211378, "BSD": 1, "BTC": 0.000083228654, "BTN": 63.826607, "BWP": 9.745417, "BYN": 1.983657, "BZD": 2.009549, "CAD": 1.246954, "CDF": 1596, "CHF": 0.959365, "CLF": 0.02275, "CLP": 605.238333, "CNH": 6.421156, "CNY": 6.420531, "COP": 2830.63, "CRC": 566.685, "CUC": 1, "CUP": 25.5, "CVE": 90.35, "CZK": 20.71545, "DJF": 178.57, "DKK": 6.084699, "DOP": 48.39, "DZD": 114.169436, "EGP": 17.7002, "ERN": 15.110528, "ETB": 27.555, "EUR": 0.817059, "FJD": 2.005257, "FKP": 0.720376, "GBP": 0.720376, "GEL": 2.567971, "GGP": 0.720376, "GHS": 4.5501, "GIP": 0.720376, "GMD": 47.66, "GNF": 9000, "GTQ": 7.34849, "GYD": 207.410107, "HKD": 7.81765, "HNL": 23.663499, "HRK": 6.0778, "HTG": 64.037492, "HUF": 251.832615, "IDR": 13407.197247, "ILS": 3.417072, "IMP": 0.720376, "INR": 63.855, "IQD": 1183, "IRR": 36009.966156, "ISK": 102.693214, "JEP": 0.720376, "JMD": 124.169382, "JOD": 0.710005, "JPY": 110.93746154, "KES": 102.874446, "KGS": 69.310501, "KHR": 4021, "KMF": 401.93, "KPW": 900, "KRW": 1070.366667, "KWD": 0.300655, "KYD": 0.833149, "KZT": 324.675966, "LAK": 8314.05, "LBP": 1512.5, "LKR": 153.88, "LRD": 127.86456, "LSL": 12.195, "LYD": 1.341607, "MAD": 9.232231, "MDL": 16.929162, "MGA": 3249.45, "MKD": 50.32, "MMK": 1347.3, "MNT": 2424.562451, "MOP": 8.050646, "MRO": 355, "MRU": 35.41, "MUR": 32.901, "MVR": 15.409873, "MWK": 726.115, "MXN": 18.637751, "MYR": 3.956402, "MZN": 58.997548, "NAD": 12.195, "NGN": 360, "NIO": 30.96, "NOK": 7.831618, "NPR": 102.128651, "NZD": 1.368675, "OMR": 0.385005, "PAB": 1, "PEN": 3.21087, "PGK": 3.23242, "PHP": 50.755, "PKR": 110.7426, "PLN": 3.400903, "PYG": 5629.4, "QAR": 3.641, "RON": 3.800111, "RSD": 96.761896, "RUB": 56.562, "RWF": 845, "SAR": 3.750982, "SBD": 7.772868, "SCR": 13.435358, "SDG": 7.025, "SEK": 8.021678, "SGD": 1.32159, "SHP": 0.720376, "SLL": 7663.973419, "SOS": 586, "SRD": 7.458, "SSP": 130.2634, "STD": 20200.635136, "STN": 20.27, "SVC": 8.748292, "SYP": 515.05499, "SZL": 12.24, "THB": 31.907, "TJS": 8.818431, "TMT": 3.50998, "TND": 2.416199, "TOP": 2.24263, "TRY": 3.769627, "TTD": 6.728571, "TWD": 29.4965, "TZS": 2244.75, "UAH": 28.795349, "UGX": 3631.45, "USD": 1, "UYU": 28.61283, "UZS": 8150, "VEF": 10.008456, "VND": 22707.282397, "VUV": 105.854483, "WST": 2.518986, "XAF": 535.955317, "XAG": 0.0587718, "XAU": 0.00075204, "XCD": 2.70255, "XDR": 0.694163, "XOF": 535.955317, "XPD": 0.00090494, "XPF": 97.501028, "XPT": 0.00099628, "YER": 250.25, "ZAR": 12.157745, "ZMW": 9.847987, "ZWL": 322.355011}

	begin 
		return "pass non-negative amount" if amount < 0
		if base_currency_rate = currency_rate[currency.upcase.to_sym]
			base_currency_exchange_rate = currency_rate.each do |k, v|
				currency_rate[k] = v/base_currency_rate
			end
			
			base_currency_exchange_rate.each do |k, v|
				base_currency_exchange_rate[k] = v*amount
			end						
		else
			return "pass the valid currency"			
		end
	rescue StandardError => e 
		e.message
	end
end

puts change_from("inr", 2)